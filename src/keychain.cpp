// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: 2023 Jorge Barroso <jorge_barroso_11 at hotmail dot com>

#include "keychain.h"

#include <QDebug>
#include <QEventLoop>

KeyChain::KeyChain(QObject *parent)
        : QObject(parent), mReadCredentialJob(QLatin1String("org.kde.kharvest")),
          mWriteCredentialJob(QLatin1String("org.kde.kharvest")),
          mDeleteCredentialJob(QLatin1String("org.kde.kharvest")) {
    mReadCredentialJob.setAutoDelete(false);
    connect(&mReadCredentialJob, &QKeychain::ReadPasswordJob::finished, this, &KeyChain::keyFinishedReading);
    mWriteCredentialJob.setAutoDelete(false);
    connect(&mWriteCredentialJob, &QKeychain::WritePasswordJob::finished, this, &KeyChain::keyFinishedStoring);
    mDeleteCredentialJob.setAutoDelete(false);
    connect(&mDeleteCredentialJob, &QKeychain::DeletePasswordJob::finished, this, &KeyChain::keyFinishedDeleting);

    connect(this, &KeyChain::keyRestored, this, &KeyChain::keyFinishedSyncReading);

    connect(this, &KeyChain::error, &syncLoop, &QEventLoop::quit);
    connect(this, &KeyChain::keyRestored, &syncLoop, &QEventLoop::quit);
}

void KeyChain::readKey(const QString &key) {
    mReadCredentialJob.setKey(key);
    mReadCredentialJob.start();
}

QString KeyChain::readKeySynchronous(const QString &key) {
    readKey(key);
    syncLoop.exec();

    return syncReadValue;
}

void KeyChain::writeKey(const QString &key, const QString &value) {
    mWriteCredentialJob.setKey(key);
    mWriteCredentialJob.setTextData(value);
    mWriteCredentialJob.start();
}

void KeyChain::deleteKey(const QString &key) {
    mDeleteCredentialJob.setKey(key);
    mDeleteCredentialJob.start();
}

void KeyChain::keyFinishedReading() {
    if (mReadCredentialJob.error()) {
        auto errorMessage = QString("%1/%2 key read failed: %3")
                .arg(mReadCredentialJob.service(),
                     mReadCredentialJob.key(),
                     qPrintable(mReadCredentialJob.errorString()));

        qDebug() << errorMessage;
        emit error(errorMessage);
    } else {
        qDebug() << QString("%1/%2 key read succeeded").arg(mReadCredentialJob.service(),
                                                            mReadCredentialJob.key());
        emit keyRestored(mReadCredentialJob.key(), mReadCredentialJob.textData());
    }
}

void KeyChain::keyFinishedStoring() {
    if (mWriteCredentialJob.error()) {
        auto errorMessage{QString("%1/%2 key write failed: %3")
                                  .arg(mWriteCredentialJob.service(),
                                       mWriteCredentialJob.key(),
                                       qPrintable(mWriteCredentialJob.errorString()))};

        qDebug() << errorMessage;
        emit error(errorMessage);
    } else {
        emit keyStored(mWriteCredentialJob.key());
    }
}

void KeyChain::keyFinishedDeleting() {
    if (mDeleteCredentialJob.error()) {
        auto errorMessage{QString("%1/%2 key delete failed: %3")
                                  .arg(mDeleteCredentialJob.service(),
                                       mDeleteCredentialJob.key(),
                                       qPrintable(mDeleteCredentialJob.errorString()))};
        qDebug() << errorMessage;
        emit error(errorMessage);
    } else {
        emit keyDeleted(mDeleteCredentialJob.key());
    }
}

void KeyChain::keyFinishedSyncReading(const QString &, const QString &textData) {
    syncReadValue = textData;
}